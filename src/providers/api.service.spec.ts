import {
  inject,
  TestBed
} from '@angular/core/testing';

import { ApiService } from './api.service';
import { CarModel } from '../models/car.model';
import { ReduxService } from './redux.service';
import { ReduxModule } from '../app/redux.module';

describe('ApiService', () => {

  let apiService: ApiService;
  const carMock = new CarModel({
    'brand': 'Audi',
    'id': '1',
    'image': 'audi-s3.png',
    'model': 'S3',
    'price': '50000',
    'fav': false
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ReduxModule],
      providers: [
        ApiService,
        ReduxService
      ]
    });

    apiService = TestBed.get(ApiService);
  });

  it('Expect instance to be created.', () => {
    expect(apiService).toBeTruthy();
  });

  it('#toggleFavCar', () => {
    expect(apiService.toggleFavCar(carMock)).toBe(true);
    expect(apiService.toggleFavCar(carMock, false)).toBe(false);
    expect(apiService.toggleFavCar(carMock, true)).toBe(true);
  });

  it('#list', () => {
    expect(apiService.list(5, 'a').length).toBe(5);
  });

});
