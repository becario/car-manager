import { Injectable } from '@angular/core';
import { NgRedux } from '@angular-redux/store';

import { IAppState } from '../app/store';

@Injectable()
export class ReduxService {

  constructor(
    public ngRedux: NgRedux<IAppState>
  ) {}

  public setReduxStateValue(value: any, action: string): void {
    this.ngRedux.dispatch({
      type: action,
      body: value
    });
  }

}

