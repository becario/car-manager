import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import { SearchResultModel } from '../models/search-result.model';
import { ApiService } from './api.service';
import { ReduxService } from './redux.service';
import { CarModel } from '../models/car.model';

@Injectable()
export class SearcherService {

  public maxResults: number;
  private searchChanged: Subject<string>;
  private searchFavChanged: Subject<any>;
  public filterBy: string;
  private orderBy: string;

  constructor(
    private apiService: ApiService,
    private reduxService: ReduxService
  ) {
    this.orderBy = 'brand';
    this.filterBy = 'brand';
    this.maxResults = 5;
    this.searchChanged = new Subject<string>();
    this.searchFavChanged = new Subject<any>();
    this.newSearchListeners();
  }

  public modelChanged(text: string, storeKey: string, filterBy?: string): void {
    if (filterBy) {
      this.searchFavChanged.next({ term: text, criteria: filterBy });
    } else {
      this.maxResults = 5;
      this.searchChanged.next(text);
    }
  }

  public newSearch(newTerm, reduxKey: string, filterBy?: string): void {
    let listStatus;
    let result = [];
    let currentResult = this.reduxService.ngRedux.getState().cars;

    if (reduxKey === 'SET_NEW_FAV_RESULTS') {
      currentResult = this.reduxService.ngRedux.getState().cars.filter((car) => car.fav);
      if (newTerm.length) {
        result = this.setFilterBy(currentResult, filterBy, newTerm);
      } else {
        result = currentResult;
      }
    } else {
      result = this.setOrderBy(this.apiService.list(this.maxResults, newTerm, currentResult));
    }
    listStatus = new SearchResultModel({items: result, term: newTerm});
    this.reduxService.setReduxStateValue(listStatus, reduxKey);
  }

  public setOrderBy(elements: CarModel[], criteria?: string): CarModel[] {
    if (Array.isArray(elements)) {
      if (criteria) {
        this.orderBy = criteria;
      }
      return elements.sort(this.compare.bind(this));
    } else {
      return [];
    }
  }

  public setFilterBy(elements: CarModel[], criteria: string, term: string): CarModel[] {
    if (Array.isArray(elements)) {
      return elements.filter((elem: CarModel) => elem[criteria].toLowerCase().indexOf(term.toLowerCase()) > -1);
    }
  }

  private newSearchListeners(): void {
    this.searchChanged.subscribe(
      (term: string) => {
        this.newSearch(term, 'SET_NEW_RESULTS');
      }
    );
    this.searchFavChanged.subscribe(
      (value: any) => {
        this.newSearch(value.term, 'SET_NEW_FAV_RESULTS', value.criteria);
      }
    );
  }

  private compare(a, b): number {
    if (a[this.orderBy] < b[this.orderBy]) {
      return -1;
    }
    if (a[this.orderBy] > b[this.orderBy]) {
      return 1;
    }

    return 0;
  }
}
