import { NgModule } from '@angular/core';
import {
  MatIconModule,
  MatListModule
} from '@angular/material';
import { MatButtonModule } from '@angular/material/button';

import { SearcherModule } from './searcher/searcher.module';

@NgModule({
  imports: [
    MatListModule,
    MatIconModule,
    MatButtonModule,
    SearcherModule
  ],
  exports: [
    MatListModule,
    MatIconModule,
    MatButtonModule
  ]
})

export class HomePageModule {

}
