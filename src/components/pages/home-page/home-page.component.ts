import { Component } from '@angular/core';

import { ReduxService } from '../../../providers/redux.service';
import { ApiService } from '../../../providers/api.service';
import { SearcherService } from '../../../providers/searcher.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent {

  constructor(
    public reduxService: ReduxService,
    public apiService: ApiService,
    public searcherService: SearcherService
  ) {}

  public seeMore(): void {
    const currentTerm = this.reduxService.ngRedux.getState().lastSearch.term;

    this.searcherService.maxResults += 5;
    this.searcherService.newSearch(currentTerm, 'SET_NEW_RESULTS');
  }
}
