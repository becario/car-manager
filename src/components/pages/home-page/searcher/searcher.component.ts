import {
  Component,
  Input,
  OnInit
} from '@angular/core';

import { ReduxService } from '../../../../providers/redux.service';
import { SearcherService } from '../../../../providers/searcher.service';

@Component({
  selector: 'app-searcher',
  templateUrl: './searcher.component.html',
  styleUrls: ['./searcher.component.scss']
})
export class SearcherComponent implements OnInit {

  @Input() public reduxKey: string;
  public storeKey: string;

  constructor(
    public reduxService: ReduxService,
    public searcherService: SearcherService
  ) {}

  public ngOnInit(): void {
    switch (this.reduxKey) {
      case 'SET_NEW_RESULTS':
        this.storeKey = 'lastSearch';
        break;
      case 'SET_NEW_FAV_RESULTS':
        this.storeKey = 'lastFavSearch';
        break;
      default:
        this.storeKey = 'lastSearch';
    }
  }

  public orderBy(criteria: string) {
    const lastSearch = this.reduxService.ngRedux.getState()[this.storeKey];
    lastSearch.items = this.searcherService.setOrderBy(lastSearch.items, criteria);

    this.reduxService.setReduxStateValue(lastSearch, this.reduxKey);
  }

  public filterBy(criteria: string) {
    const lastSearch = this.reduxService.ngRedux.getState()[this.storeKey];
    const term = this.reduxService.ngRedux.getState()[this.storeKey].term;
    lastSearch.items = this.searcherService.setFilterBy(lastSearch.items, criteria, term);
    this.searcherService.filterBy = criteria;

    this.reduxService.setReduxStateValue(lastSearch, this.reduxKey);
  }

  public searchTermChange(): void {
    if (this.reduxKey === 'SET_NEW_FAV_RESULTS') {
      this.searcherService.modelChanged(
        this.reduxService.ngRedux.getState()[this.storeKey].term,
        this.storeKey,
        this.searcherService.filterBy
      );
    } else {
      this.searcherService.modelChanged(
        this.reduxService.ngRedux.getState()[this.storeKey].term,
        this.storeKey
      );
    }

  }

}
