import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material';

import { SearcherComponent } from './searcher.component';
import {CommonModule} from '@angular/common';

@NgModule({
  declarations: [ SearcherComponent ],
  imports: [
    CommonModule,
    FormsModule,
    MatInputModule
  ],
  exports: [
    MatInputModule,
    SearcherComponent
  ]
})

export class SearcherModule {

}
