import { Injectable } from '@angular/core';
import { FavsPageComponent } from '../../favs-page/favs-page.component';
import { MatDialog } from '@angular/material';

@Injectable()
export class BottomOptionsService {

  constructor(public dialog: MatDialog) {}

  public showFavsModal(): void {
    this.dialog.open(FavsPageComponent, {
      width: 'calc(100% - 20px)',
      maxHeight: '80vh',
      maxWidth: '500px'
    });
  }
}
