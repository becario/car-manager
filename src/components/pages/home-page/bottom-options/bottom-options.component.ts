import { Component } from '@angular/core';
import { BottomOptionsService } from './bottom-options.service';

@Component({
  selector: 'app-bottom-options',
  templateUrl: './bottom-options.component.html',
  styleUrls: ['./bottom-options.component.scss']
})
export class BottomOptionsComponent {

  constructor(
    public bottomOptionsService: BottomOptionsService
  ) { }

}
