import { NgModule } from '@angular/core';
import {MatDialogModule} from '@angular/material';

import { BottomOptionsComponent } from './bottom-options.component';
import { BottomOptionsService } from './bottom-options.service';
import { FavsPageModule } from '../../favs-page/favs-page.module';

@NgModule({
  declarations: [ BottomOptionsComponent ],
  imports: [
    MatDialogModule,
    FavsPageModule
  ],
  providers: [ BottomOptionsService ],
  exports: [
    MatDialogModule,
    BottomOptionsComponent
  ]
})

export class BottomOptionsModule {

}
