import { Injectable } from '@angular/core';

import { CarModel } from '../../../models/car.model';
import { ReduxService } from '../../../providers/redux.service';

@Injectable()
export class FavsPageService {

  constructor(private reduxService: ReduxService) {}

  public getFavCount(): number {
    const cars = this.reduxService.ngRedux.getState().cars;

    return cars.filter((car: CarModel) => car.fav).length;
  }

}
