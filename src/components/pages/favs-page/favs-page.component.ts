import {
  Component,
  OnInit
} from '@angular/core';

import { ReduxService } from '../../../providers/redux.service';
import { FavsPageService } from './favs-page.service';
import { ApiService } from '../../../providers/api.service';
import { CarModel } from '../../../models/car.model';

@Component({
  selector: 'app-favs-page',
  templateUrl: './favs-page.component.html',
  styleUrls: ['./favs-page.component.scss']
})
export class FavsPageComponent implements OnInit {

  public favCarsCount: number;

  constructor(
    public reduxService: ReduxService,
    public apiService: ApiService,
    private favsPageService: FavsPageService
  ) {
    this.favCarsCount = 0;
  }

  public ngOnInit(): void {
    this.totalCount();
  }

  public removeFav(car: CarModel): void {
    this.apiService.toggleFavCar(car, false);
    this.totalCount();
  }

  private totalCount(): void {
    this.favCarsCount = this.favsPageService.getFavCount();
  }

}
