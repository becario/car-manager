import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatIconModule,
  MatListModule
} from '@angular/material';

import { FavsPageComponent } from './favs-page.component';
import { FavsPageService } from './favs-page.service';
import { SearcherModule } from '../home-page/searcher/searcher.module';

@NgModule({
  declarations: [FavsPageComponent],
  imports: [
    CommonModule,
    MatListModule,
    MatIconModule,
    SearcherModule
  ],
  exports: [FavsPageComponent],
  providers: [FavsPageService],
  entryComponents: [FavsPageComponent]
})
export class FavsPageModule {

}
