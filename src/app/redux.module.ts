import { NgModule } from '@angular/core';
import {
  NgRedux,
  NgReduxModule,
  DevToolsExtension
} from '@angular-redux/store';

import {
  IAppState,
  rootReducer,
  INITIAL_STATE
} from './store';
import {ReduxService} from '../providers/redux.service';

@NgModule({
  imports: [NgReduxModule],
  providers: [ReduxService]
})
export class ReduxModule {

  constructor(
    devTools: DevToolsExtension,
    public ngRedux: NgRedux<IAppState>
  ) {
    ngRedux.configureStore(
      rootReducer,
      INITIAL_STATE,
      [],
      devTools.isEnabled() ? [ devTools.enhancer() ] : []
    );
  }

}
