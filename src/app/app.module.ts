import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  RouterModule,
  Routes
} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ReduxModule } from './redux.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from '../components/pages/home-page/home-page.component';
import { ApiService } from '../providers/api.service';
import { HomePageModule } from '../components/pages/home-page/home-page.module';
import { SearcherModule } from '../components/pages/home-page/searcher/searcher.module';
import { BottomOptionsModule } from '../components/pages/home-page/bottom-options/bottom-options.module';
import { SearcherService } from '../providers/searcher.service';

const appRoutes: Routes = [
  { path: 'fav-cars', loadChildren: '../components/pages/favs-page/favs-page.module#FavsPageModule', pathMatch: 'full' },
  { path: '', component: HomePageComponent, pathMatch: 'full' },
  { path: '**', loadChildren: '../components/pages/page-not-found/page-not-found.module#PageNotFoundModule' }
];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true }
    ),
    ReduxModule,
    HomePageModule,
    SearcherModule,
    BottomOptionsModule
  ],
  providers: [
    ApiService,
    SearcherService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
