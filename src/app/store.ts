import { CarModel } from '../models/car.model';
import { SearchResultModel } from '../models/search-result.model';

export interface IAppState {
  cars: CarModel[];
  lastSearch: SearchResultModel;
  lastFavSearch: SearchResultModel;
}

export const INITIAL_STATE: IAppState = {
  cars: [],
  lastSearch: new SearchResultModel({
    items: [],
    term: ''
  }),
  lastFavSearch: new SearchResultModel({
    items: [],
    term: ''
  })
};

export function rootReducer(state: IAppState, action: any): IAppState {

  switch (action.type) {
    case 'ADD_CAR':
      state.cars.push(action.body);
      break;
    case 'UPDATE_CAR':
      const carId = action.body.id;
      state.cars = state.cars.map(
        (car: CarModel) => {
          if (car.id === carId) {
            return action.body;
          } else {
            return car;
          }
        }
      );
      break;
    case 'SET_NEW_RESULTS':
      state.lastSearch = action.body;
      break;
    case 'SET_NEW_FAV_RESULTS':
      state.lastFavSearch = action.body;
      break;
    case 'ADD_FAV':
      state.lastFavSearch.items.push(action.body);
      break;
    case 'REM_FAV':
      state.lastFavSearch.items = state.lastFavSearch.items.filter((car) => car.id !== action.body.id);
      break;
  }

  return state;

}
