export class CarModel {

  public id: number;
  public brand: string;
  public image: string;
  public model: string;
  public price: number;
  public fav: boolean;

  constructor(args?: any) {
    Object.assign(this, ...args);
  }

}
