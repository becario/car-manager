import { CarModel } from './car.model';

export class SearchResultModel {

  public items: CarModel[];
  public term: string;

  constructor(args?: any) {
    Object.assign(this, ...args);
  }

}
